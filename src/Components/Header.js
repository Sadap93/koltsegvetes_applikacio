import { useTranslation } from "react-i18next";
import i18n from "i18next";

const Header = () => {
  const { t } = useTranslation();
  const changeLanguageToHu = () => {
    i18n.changeLanguage("hu");
  }
  const changeLanguageToEn = () => {
    i18n.changeLanguage("en");
  }

  return (
    <div className="container mt-2">
      <div className="row mt-2">
        <div className="col-sm-2">
          <select className="form-select" aria-label="Default select example">
            <option value="1">2023</option>
            <option value="2">2022</option>
          </select>
        </div>
        <div className="col-sm-2">
          <select className="form-select" aria-label="Default select example">
            <option value="1">{t("january")}</option>
            <option value="2">{t("february")}</option>
            <option value="3">{t("march")}</option>
            <option value="4">{t("april")}</option>
            <option value="5">{t("may")}</option>
            <option value="6">{t("june")}</option>
            <option value="7">{t("july")}</option>
            <option value="8">{t("august")}</option>
            <option value="9">{t("september")}</option>
            <option value="10">{t("october")}</option>
            <option value="11">{t("november")}</option>
            <option value="12">{t("december")}</option>
          </select>
        </div>
        <div className="col-auto">
          <button onClick={changeLanguageToHu}>HU</button>
        </div>
        <div className="col-auto">
          <button onClick={changeLanguageToEn}>EN</button>
        </div>
      </div>
    </div>
  );
};

export default Header;
